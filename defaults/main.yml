---
# Dependent apt packages
magicmirror_apt_packages:
  - curl
  - wget
  - git
  - build-essential
  - unzip
  - unclutter
  - x11-xserver-utils
  # - nodejs
  # - npm
  # # alexa related
  # - sox
  # - libsox-fmt-all
  # - swig3.0
  # - python-pyaudio
  # - python3-pyaudio
  # - libatlas-base-dev
  # # end alexa related

# Audio output device, options- 0:automatic 2:hdmi 1:analogue (headphone jack)
magicmirror_audio_output_device: 0

# Audio capture device
magicmirror_audio_capture_device: 'hw:1,0'

# User account to use pm2 with
magicmirror_pm2_user: pi

# Parent directory to checkout MagicMirror repository
magicmirror_src_dir: /home/{{ magicmirror_pm2_user }}/src

# Minimal nodejs version string
magicmirror_min_nodejs_version: v5.1.0

# Stable branch name for nodejs installer
magicmirror_nodejs_stable_branch: 16.x

# Used internally in role to set whether nodejs needs to be upgraded
magicmirror_nodejs_upgrade_needed: no

# Path to plymouth themes directory for MagicMirror
magicmirror_plymouth_theme_dir: /usr/share/plymouth/themes/MagicMirror

# 12 or 24 hr time format
magicmirror_timeformat: 24

# imperial or metric units
magicmirror_units: metric

# Default timezone
magicmirror_timezone: Europe/London

# API key from http://www.openweathermap.org
magicmirror_weather_api_key: 635bffa245d6fbbeca8441d7df2749b4

# Header for calendar module
magicmirror_calendar_header: US Holidays

# Calendar module 'location' value
magicmirror_weather_location: Cheltenham

# ID from http://www.openweathermap.org/help/city_list.txt
magicmirror_weather_locationid: 2653261

# List of newsfeeds
magicmirror_newsfeeds:
  - { title: "BBC", url: "http://feeds.bbci.co.uk/news/world/rss.xml" }
  - { title: "The Guardian", url: "http://theguardian.com/world/rss" }

# Hide private events
magicmirror_calendar_hideprivate: false

# List of calendars
magicmirror_calendars:
  - { symbol: "calendar-check-o ", url: "https://calendar.google.com/calendar/ical/uk%40holiday.calendar.google.com/public/basic.ics" }
  - { symbol: "calendar-check-o ", url: "https://calendar.google.com/calendar/ical/foster63%40gmail.com/private-d29218e533b0790bee4aa33d1ff2a707/basic.ics" }

# External json file for compliments module
magicmirror_compliments_file:

# List of third party modules clone url's, example syntax
magicmirror_extra_modules:
  - name: MMM-MQTT
    url: https://github.com/ottopaulsen/MMM-MQTT.git
    npm_install: yes
  - name: MMM-Tools
    url: https://github.com/bugsounet/MMM-Tools.git
    npm_install: yes

# Whether to add watchdog module in config
magicmirror_watchdog_enabled: true

# Name of pm2 app to restart on failure
magicmirror_watchdog_pm2_app: 'MagicMirror'

# Whether to add systemstats module in config
magicmirror_systemstats_enabled: true

# Header for SystemStats module
magicmirror_systemstats_header: System Stats

# Update Interval in ms, defaults to 10s
magicmirror_systemstats_updateinterval: 10000

# Whether to add MyCommute module in config
magicmirror_mycommute_enabled: false

# Header for mycommute module
magicmirror_mycommute_header: Traffic

# Google API key, https://developers.google.com/maps/documentation/javascript/get-api-key
magicmirror_mycommute_api_key:

# Starting address (home)
magicmirror_mycommute_origin:

# Start time when module will be visible (24-hr format, defaults to midnight)
magicmirror_mycommute_starttime: '00:00'

# End time when module will be visible (24hr format, defaults to one minute before midnight)
magicmirror_mycommute_endtime: '23:59'

# Array of days to hide module. Valid numbers are 0 through 6, 0 = Sunday, 6 = Saturday.
magicmirror_mycommute_hidedays:

# Format of total travel time
magicmirror_mycommute_traveltimeformat: m [min]

# How often to poll for traffic updates in milliseconds. Defaults to 10 minutes (10 * 60 * 1000)
magicmirror_mycommute_pollfrequency: 600000

# Array of destinations
magicmirror_mycommute_destinations:

# Whether to add alexa module in config
magicmirror_alexa_enabled: false

# Alexa AVS client id
magicmirror_alexa_clientid:

# Alexa AVS client secret
magicmirror_alexa_clientsecret:

# Alexa AVS device id
magicmirror_alexa_deviceid:

# Alexa AVS refresh token
magicmirror_alexa_refreshtoken:

# Alexa AVS wake word
magicmirror_alexa_wakeword: Smart Mirror

# Whether to add the PIR sensor module in config
magicmirror_pirsensor_enabled: false

# BCM pin number sensor's digital out is connected to
magicmirror_pirsensor_pin: 22

# Turn off HDMI port when no motion?
magicmirror_pirsensor_powersaving: true

# Delay befor turning off HDMI port in seconds
magicmirror_pirsensor_powersaving_delay: 10
